<?php


/** HOW TO USE */
/**
    // add this code in public/index.php or similar entry point

    if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/sdebug/index.php')) {
        include_once $_SERVER['DOCUMENT_ROOT'] . '/sdebug/index.php';
    } else {
        function sd($var)
        {
            return false;
        }
    }
 */
/**
 then use func sd($var)
 results on yourproject.url/sdebug/
*/

// php errors enable
error_reporting(E_ALL);
ini_set('display_errors', 0);
ini_set('log_errors', 'on');
ini_set('error_log', $_SERVER['DOCUMENT_ROOT'].'/sdebug/php_errors.log');


if (!isset($argv)) {
    $server_root = str_replace('\\', '/', $_SERVER['DOCUMENT_ROOT']);
    $current_dir = str_replace('\\', '/', getcwd());
    if ($server_root !== $current_dir && $server_root . '/sdebug' !== $current_dir) {
        $path = str_replace('/sdebug', '', str_replace($_SERVER['DOCUMENT_ROOT'], '', getcwd()));
        $_SERVER['DOCUMENT_ROOT'] = $_SERVER['DOCUMENT_ROOT'] . $path;
    }
}

$html_path = $_SERVER['DOCUMENT_ROOT'] . '/sdebug/debug.html'; // TODO

error_reporting(E_ERROR | E_PARSE);
$html_debug_template = <<<EX
<!DOCTYPE html>
<html lang="ru">
<head>
<link rel="shortcut icon" href="/sdebug/favicon.ico">
    <script src="/sdebug/highlight.min.js"></script>
<script>hljs.initHighlightingOnLoad();</script>
<script>
function idea(path, line){
     const request = new XMLHttpRequest();
     const url = `http://localhost:63342/api/file/?file=` + path + `&line=` + line;
     request.open('GET', url);
     request.send();
}
</script>
<meta charset="utf-8">
<title>debug info</title></head>
<div><h2>Super Debug v0.121</h2><h6></h6>
    <div class="sWitcher"> Dark mode
        <label class="switch">
            <input type="checkbox" checked>
            <span class="slider round"></span>
        </label></div></div><form action="{$path}/sdebug/" method="post">
<input type="submit" name="refresh" value="Refresh"/> <input type="submit" name="clear" value="Clear"/>
</form>
<body>
</body>
</html>


<style>
    .inline {
        display: inline;
    }

    html {
        background-color: #171c20;
    }

    h6, h2 {
        display: inline-block;
    }

    details {
        background-color: #1e1e1e;
    }

    .details {
        padding-left: 20px;
        border: inset;
        margin-bottom: 20px;
    }

    summary {
        cursor: pointer;
        user-select: none;
        -moz-user-select: none;
        /*-khtml-user-select: none;*/
        -webkit-user-select: none;
        /*-o-user-select: none;*/
        outline: none;
        background-color: #22272b;
        padding-bottom: 5px;
    }

    body {
        font-family: 'Times New Roman';
        max-width: 1280px;
        background-color: #22272b;
        display: block;
        margin-left: auto;
        margin-right: auto;
        padding-left: 30px;
        padding-right: 30px;
        padding-top: 5px;
        padding-bottom: 15px;
        border: double;
        overflow-y: scroll;
        border-radius: 10px;
    }

    input {
        background: #c3c3c3;
        border-color: #bdbdbd;
        border-style: groove;
        border-radius: 4px;
        color: #22272b;
        font-weight: bold;
        padding: 5px;
        padding-left: 10px;
        padding-right: 10px;
        margin: 2px;
    }

    form {
        margin-bottom: 20px;
    }

    .red {
        color: red !important;
    }

    .green {
        color: green !important;
    }

    .summary_buttons {
        padding: 0 !important;
        padding-left: 10px !important;
        padding-right: 10px !important;
    }

    button {
        background: #eafe9c;
        border-color: #97905a;
        border-style: groove;
        border-radius: 4px;
        color: #22272b;
        font-weight: bold;
    }

    * {
        color: #c3c3c3;

    }


    .switch {
        position: relative;
        display: inline-block;
        width: 50px;
        height: 24px;
    }

    .switch input {
        opacity: 0;
        width: 0;
        height: 0;
    }

    .slider {
        position: absolute;
        cursor: pointer;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: #ccc;
        -webkit-transition: .4s;
        transition: .4s;
    }

    .slider:before {
        position: absolute;
        content: "";
        height: 16px;
        width: 16px;
        left: 4px;
        bottom: 4px;
        background-color: white;
        -webkit-transition: .4s;
        transition: .4s;
    }

    input:checked + .slider {
        background-color: #2196F3;
    }

    input:focus + .slider {
        box-shadow: 0 0 1px #2196F3;
    }

    input:checked + .slider:before {
        -webkit-transform: translateX(26px);
        -ms-transform: translateX(26px);
        transform: translateX(26px);
    }

    /* Rounded sliders */
    .slider.round {
        border-radius: 34px;
    }

    .slider.round:before {
        border-radius: 50%;
    }

    .sWitcher {
        margin-top: -44px;
        text-align: right;
    }

    .hljs {
      display: block;
      overflow-x: auto;
      padding: 0.5em;
      background: #1e1e1e;
      color: #dcdcdc;
    }
    .hljs-keyword,
    .hljs-literal,
    .hljs-symbol,
    .hljs-name {
      color: #569cd6;
    }
    .hljs-link {
      color: #569cd6;
      text-decoration: underline;
    }
    .hljs-built_in,
    .hljs-type {
      color: #4ec9b0;
    }
    .hljs-number,
    .hljs-class {
      color: #b8d7a3;
    }
    .hljs-string,
    .hljs-meta-string {
      color: #d69d85;
    }
    .hljs-regexp,
    .hljs-template-tag {
      color: #9a5334;
    }
    .hljs-subst,
    .hljs-function,
    .hljs-title,
    .hljs-params,
    .hljs-formula {
      color: #dcdcdc;
    }
    .hljs-comment,
    .hljs-quote {
      color: #57a64a;
      font-style: italic;
    }
    .hljs-doctag {
      color: #608b4e;
    }
    .hljs-meta,
    .hljs-meta-keyword,
    .hljs-tag {
      color: #9b9b9b;
    }
    .hljs-variable,
    .hljs-template-variable {
      color: #bd63c5;
    }
    .hljs-attr,
    .hljs-attribute,
    .hljs-builtin-name {
      color: #9cdcfe;
    }
    .hljs-section {
      color: gold;
    }
    .hljs-emphasis {
      font-style: italic;
    }
    .hljs-strong {
      font-weight: bold;
    }
    .hljs-bullet,
    .hljs-selector-tag,
    .hljs-selector-id,
    .hljs-selector-class,
    .hljs-selector-attr,
    .hljs-selector-pseudo {
      color: #d7ba7d;
    }
    .hljs-addition {
      background-color: #144212;
      display: inline-block;
      width: 100%;
    }
    .hljs-deletion {
      background-color: #600;
      display: inline-block;
      width: 100%;
    }


</style>
EX;

/**
 * @param $var
 *
 * @throws Exception
 */
function sd($var)
{

    $bt = debug_backtrace(); // Получаем инфу откуда был вызван дебаг
    $bt = array_shift($bt); // Теперь вся инфа в первом элементе массива
    $file = file($bt['file']); // Читаем файл что бы достать из него название переменной
    $src = $file[$bt['line'] - 1]; // Получаем номер строки откуда вызывалась функция дебага
    $pattern = '#(.*)' . __FUNCTION__ . ' *?\( *?(.*) *?\)(.*)#i'; // Регулярка для выдирания $var
    $from = substr($bt['file'], strrpos($bt['file'], '\\') + 1);
    $is_json = false;

    if (is_string($var)) {
        // Если это жсон то делаем его красивым
        if (is_object(json_decode($var))) {
            $var = json_decode($var, true);
            $var = json_encode($var, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
            $is_json = true;
        }
    }

    // Формируем debug_info
    $debug_info['var_name'] = preg_replace($pattern, '$2', $src); // Отбрасываем лишнее
    $debug_info['exec_line'] = $bt['line'];
    $debug_info['file_name'] = $from;
    $debug_info['file_path'] = str_replace('\\', '/', $bt['file']);
    $debug_info['content'] = $var;

    // Создаём класс дива
    $html_id = $debug_info['file_name'] . '-' . $debug_info['exec_line'] . '-' . $debug_info['var_name'];
    $html_id = str_replace('$', '', $html_id);
    $html_id = str_replace('.php', '', $html_id);

    createHTML(); // Создаём хтмл файл если его нет

    // Открываем debug.html, загружаем в переменную
    $html_file = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/sdebug/debug.html');

    $matches = [];
    preg_match_all('#<div class="date inline"[^>]*>(.*?)</div>#s', $html_file, $matches);

    if (!isset($matches[1][0])) {
        $mat = date("Y-m-d H:i:s.") . gettimeofday()["usec"];
    }

    $string_date_first = ($mat ?? null) ?: end($matches[1]);
    $ms_first = round('0.' . substr($string_date_first, strpos($string_date_first, ".") + 1), 2);

    $string_date_current = date("Y-m-d H:i:s.") . gettimeofday()["usec"];
    $ms_current = round('0.' . substr($string_date_current, strpos($string_date_current, ".") + 1), 2);

    $date_first = new DateTime(end($matches[1]));
    $date_current = new DateTime(date("Y-m-d H:i:s.") . gettimeofday()["usec"]);
    $diffInSeconds = $date_current->getTimestamp() - $date_first->getTimestamp();
    if ($diffInSeconds >= 10) {
        $duration = 'red';
    } else {
        $duration = 'green';
    }
    $diffInSeconds = $diffInSeconds + $ms_current - $ms_first;


    // Формируем хтмл для встройки на страницу
    $html_debug_content = '
            <div class="element" id="' . $html_id . '">
                <details open>
                    <summary> |
                        File <b>' . $debug_info['file_name'] . '</b> |
                        Exec from line <b>' . $debug_info['exec_line'] . '</b>
                        <button onclick="idea(\'' . $debug_info['file_path'] . '\',' . $debug_info['exec_line'] . ')">IDEA</button>
                        Variable <b>' . $debug_info['var_name'] . '</b> |
                        Date <b><div class="date inline">' . date("Y-m-d H:i:s.") . gettimeofday()["usec"] . '</div></b> |
                        Duration <b><div class="' . $duration . ' inline">+' . $diffInSeconds . '</div></b> |
                        <input type="submit" name="clear" value="Copy" class="summary_buttons"><input type="submit" name="clear" value="Copy as Json" class="summary_buttons">
                    </summary>
                    <div class="details"><pre><code>' . print_r($debug_info['content'], true) . '</code></pre></div>
                </details>
            </div>';

    // magic
// ======================================================================================================================
// Взять $html_file и upsert'нуть его $html_debug_content'ом

    //Проверяем есть ли такой элемент с таким айди
    if (strpos(btw($html_file), btw($html_id))) {
        // Апдейтим если есть
        $html_file = str_replace("<body>", '<body>' . $html_debug_content, $html_file);
    } else {
        //Добавляем если нет
        $html_file = str_replace("<body>", '<body>' . $html_debug_content, $html_file);
    }

// ======================================================================================================================
    // Пишем в хтмл обновлённую страницу
    file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/sdebug/debug.html', print_r($html_file, true));
//    print_r($html_file);
}

//print_r($_POST);

//if ($_SERVER['REQUEST_METHOD'] == "POST") {
if (isset($_POST['clear'])) {
    file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/sdebug/debug.html', print_r($GLOBALS['html_debug_template'], true));
    header("Refresh:0;");
}
if (isset($_POST['refresh'])) {
    header("Refresh:0;");
}
//}

$url = $_SERVER['REQUEST_URI'];

if (preg_match("/.*sdebug*/", $url)) {
    createHTML();
    require_once($_SERVER['DOCUMENT_ROOT'] . '/sdebug/debug.html');
//    file_put_contents($_SERVER['DOCUMENT_ROOT'].'/sdebug/debug.html', print_r($GLOBALS['html_debug_template'], true));
}

function btw($b1)
{
    $b1 = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $b1);
    $b1 = str_replace(["\r\n", "\r", "\n", "\t", '  ', '    ', '    '], '', $b1);

    return $b1;
}

function createHTML()
{
    // Проверяем существование debug.html
    if (!file_exists($_SERVER['DOCUMENT_ROOT'] . '/sdebug/debug.html')) {

        // Создаём если его нет
        touch($_SERVER['DOCUMENT_ROOT'] . '/sdebug/debug.html');

        // И записываем шаблон страницы
        file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/sdebug/debug.html', print_r($GLOBALS['html_debug_template'], true));

    }
}
