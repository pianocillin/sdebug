# Super Debug

## How to use

Add this code in public/index.php or similar entry point

    if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/sdebug/index.php')) {
        include_once $_SERVER['DOCUMENT_ROOT'] . '/sdebug/index.php';
    } else {
        function sd($var)
        {
            return false;
        }
    }

Then use func sd($var)

You can see and manage results on ```yourproject.url/sdebug/```

![img.png](readme.png)